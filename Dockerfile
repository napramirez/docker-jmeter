FROM openjdk:8-jdk

ARG JMETER_URL=https://downloads.apache.org//jmeter/binaries/apache-jmeter-5.3.tgz

RUN apt update && apt install -y libswt-gtk-4-java

RUN mkdir -p /usr/share/apache-jmeter
RUN curl -sLo - $JMETER_URL | tar zxv -C /usr/share/apache-jmeter --strip-components=1

RUN useradd -m jmeteruser -s /bin/bash
USER jmeteruser
WORKDIR /home/jmeteruser

CMD ["/usr/share/apache-jmeter/bin/jmeter"]
